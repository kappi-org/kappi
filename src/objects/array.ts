import {
  Handler,
  notifyChange,
  terminate,
  TerminatorGraph,
  WatchCallback,
  watchGraph,
  watchKey,
} from '../internals';
import { KeyGraph } from '../key-graph/types';
import { eachSymbol } from '../symbols/each';

declare global {
  interface Array<T> {
    [eachSymbol]: Handler;
  }
}

const enum ArrayChangeType {
  Basic,
  PushMany,
  PopMany,
}

interface ArrayChangeMeta {
  i: number;
  t?: ArrayChangeType;
}

Array.prototype[eachSymbol] =
function arrayWatchEach (callback: WatchCallback, tail?: KeyGraph, nextTail?: KeyGraph) {
  if (!tail) {
    return watchKey(this, eachSymbol, callback);
  }

  const tMap = new Map<number, { o: object; t: TerminatorGraph }>();
  const t2 = new Set<TerminatorGraph>();

  const eachTerminator = watchKey(this, eachSymbol, ({ i, t }: ArrayChangeMeta) => {
    if (t) {
      if (t === ArrayChangeType.PushMany) {
        const end = this.length;

        while (i < end) {
          const value = this[i];

          if (typeof value === 'object' && value) {
            const _t = watchGraph(
              callback, value, tail, nextTail
            );
            t2.add(_t);
            tMap.set(i, { o: value, t: _t });
          }

          i++;
        }
      }
      else if (t === ArrayChangeType.PopMany) {
        const end = this.length;

        while (i > end) {
          i--;

          const oldRecord = tMap.get(i);

          if (oldRecord) {
            const _t = oldRecord.t;
            terminate(_t);
            t2.delete(_t);
            tMap.delete(i);
          }
        }
      }
    }
    else {
      const oldRecord = tMap.get(i);
      const currentValue = this[i];

      if (oldRecord) {
        if (oldRecord.o !== currentValue) {
          const _t = oldRecord.t;
          terminate(_t);
          t2.delete(_t);
          tMap.delete(i);
        }
      }
      else if (typeof currentValue === 'object' && currentValue) {
        const _t = watchGraph(
          callback, currentValue, tail, nextTail
        );
        t2.add(_t);
        tMap.set(i, { o: currentValue, t: _t });
      }
    }
  });

  for (const [ i, _obj ] of this.entries()) {
    if (typeof _obj === 'object' && _obj) {
      const _t = watchGraph(
        callback, _obj, tail, nextTail
      );
      t2.add(_t);
      tMap.set(i, { o: _obj, t: _t });
    }
  }

  return [
    eachTerminator,
    watchKey(this, eachSymbol, callback),
    t2
  ];
};

export function put <T extends unknown[], K extends keyof T & number> (
  array: T, index: K, value: T[K]
): T[K] {
  const oldValue = array[index];
  array[index] = value;

  if (array[index] !== oldValue) {
    notifyChange(array, eachSymbol, { i: index });
  }

  return value;
}

export function push <T extends unknown[], K extends keyof T & number> (
  array: T, value: T[K]
): number {
  const pushedIndex = array.length;
  array.push(value);

  notifyChange(array, eachSymbol, { i: pushedIndex });

  return array.length;
}

export function pushMany <T extends unknown[], K extends keyof T & number> (
  array: T, values: T[K][]
): number {
  const i = array.length;
  array.push(...values);

  notifyChange(array, eachSymbol, { t: ArrayChangeType.PushMany, i });

  return array.length;
}

export function pop <T extends unknown[], K extends keyof T & number> (
  array: T
): T[K] {
  const result = array.pop() as T[K];

  notifyChange(array, eachSymbol, { i: array.length });

  return result;
}

export function popMany <T extends unknown[], K extends keyof T & number> (
  array: T, count: number
): T[K][] {
  const i = array.length;
  const results: T[K][] = [];
  while (count && array.length) {
    results.push(array.pop() as T[K]);
    count--;
  }

  notifyChange(array, eachSymbol, { t: ArrayChangeType.PopMany, i });

  return results;
}
