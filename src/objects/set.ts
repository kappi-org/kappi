import {
  Handler,
  notifyChange,
  terminate,
  TerminatorGraph,
  WatchCallback,
  watchGraph,
  watchKey,
} from '../internals';
import { KeyGraph } from '../key-graph/types';
import { eachSymbol } from '../symbols/each';
import { eachKeySymbol } from '../symbols/each-key';

declare global {
  interface Set <T> {
    [eachSymbol]: Handler;
  }
}

const enum SetChangeType {
  Basic,
  Clear
}

interface SetChangeMeta {
  v?: unknown;
  t?: SetChangeType;
}

Set.prototype[eachKeySymbol] =
Set.prototype[eachSymbol] =
function setWatchEach (callback: WatchCallback, tail?: KeyGraph, nextTail?: KeyGraph) {
  if (!tail) {
    return watchKey(this, eachSymbol, callback);
  }

  const tMap = new Map<object, TerminatorGraph>();
  const t2 = new Set<TerminatorGraph>();

  const eachTerminator = watchKey(this, eachSymbol, ({ v, t }: SetChangeMeta) => {
    if (t) {
      // handles clear

      terminate(t2);
      t2.clear();
      tMap.clear();
    }
    else {
      const oldT = tMap.get(v as object);
      const hasKey = this.has(v);

      if (!(oldT && hasKey)) {
        if (oldT) {
          // handles deltetion of key

          terminate(oldT);
          t2.delete(oldT);
          tMap.delete(v as object);
        }
        else if (hasKey && typeof v === 'object' && v) {
          // handles a new object key

          const _t = watchGraph(
            callback, v, tail, nextTail
          );
          t2.add(_t);
          tMap.set(v, _t);
        }
      }
    }
  });

  for (const v of this.values()) {
    if (typeof v === 'object' && v) {
      const _t = watchGraph(
        callback, v, tail, nextTail
      );
      t2.add(_t);
      tMap.set(v, _t);
    }
  }

  return [
    eachTerminator,
    watchKey(this, eachSymbol, callback),
    t2
  ];
};

export function setAdd <V, T extends Set<V>> (
  set: T, value: V
): void {
  if (!set.has(value)) {
    set.add(value);

    notifyChange(set, eachSymbol, { v: value });
  }
}

export function setDelete <V, T extends Set<V>> (
  set: T, value: V
): boolean {
  const result = set.delete(value);

  if (result) {
    notifyChange(set, eachSymbol, { v: value });
  }

  return result;
}

export function setClear <T extends Set<unknown>> (
  set: T,
): void {
  if (set.size) {
    set.clear();

    notifyChange(set, eachSymbol, { t: SetChangeType.Clear });
  }
}

export class WatchableSet <T> extends Set<T> {
  add (value: T): this {
    if (this.has(value)) {
      super.add(value);

      notifyChange(this, eachSymbol, { v: value });
    }

    return this;
  }
  delete (value: T): boolean {
    const result = super.delete(value);

    if (result) {
      notifyChange(this, eachSymbol, { v: value });
    }

    return result;
  }
  clear (): void {
    if (this.size) {
      super.clear();

      notifyChange(this, eachSymbol, { t: SetChangeType.Clear });
    }
  }
}
