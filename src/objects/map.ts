import {
  Handler,
  notifyChanges,
  terminate,
  TerminatorGraph,
  WatchCallback,
  watchGraph,
  watchKey,
} from '../internals';
import { KeyGraph } from '../key-graph/types';
import { eachSymbol } from '../symbols/each';
import { eachKeySymbol } from '../symbols/each-key';

declare global {
  interface Map <K, V> {
    [eachSymbol]: Handler;
  }
}

const enum MapChangeType {
  Basic,
  Clear
}

interface MapChangeMeta {
  k?: unknown;
  t?: MapChangeType;
}

Map.prototype[eachSymbol] =
function mapWatchEach (callback: WatchCallback, tail?: KeyGraph, nextTail?: KeyGraph) {
  if (!tail) {
    return watchKey(this, eachSymbol, callback);
  }

  const tMap = new Map<unknown, { o: object; t: TerminatorGraph }>();
  const t2 = new Set<TerminatorGraph>();

  const eachTerminator = watchKey(this, eachSymbol, ({ k, t }: MapChangeMeta) => {
    if (t) {
      // handles clear

      terminate(t2);
      t2.clear();
      tMap.clear();
    }
    else {
      const oldRecord = tMap.get(k);
      const currentValue = this.get(k);

      if (oldRecord) {
        if (oldRecord.o !== currentValue) {
          // handles deletion / new value

          const _t = oldRecord.t;
          terminate(_t);
          t2.delete(_t);
          tMap.delete(k);
        }
      }
      else if (typeof currentValue === 'object' && currentValue) {
        // handles new object value

        const _t = watchGraph(
          callback, currentValue, tail, nextTail
        );
        t2.add(_t);
        tMap.set(k, { o: currentValue, t: _t });
      }
    }
  });

  for (const [ k, _obj ] of this.entries()) {
    if (typeof _obj === 'object' && _obj) {
      const _t = watchGraph(
        callback, _obj, tail, nextTail
      );
      t2.add(_t);
      tMap.set(k, { o: _obj, t: _t });
    }
  }

  return [
    eachTerminator,
    watchKey(this, eachSymbol, callback),
    t2
  ];
};

Map.prototype[eachKeySymbol] =
function mapWatchEach (callback: WatchCallback, tail?: KeyGraph, nextTail?: KeyGraph) {
  if (!tail) {
    return watchKey(this, eachKeySymbol, callback);
  }

  const tMap = new Map<object, TerminatorGraph>();
  const t2 = new Set<TerminatorGraph>();

  const eachTerminator = watchKey(this, eachKeySymbol, ({ k, t }: MapChangeMeta) => {
    if (t) {
      // handles clear

      terminate(t2);
      t2.clear();
      tMap.clear();
    }
    else {
      const oldT = tMap.get(k as object);
      const hasKey = this.has(k);

      if (!(oldT && hasKey)) {
        if (oldT) {
          // handles deltetion of key

          terminate(oldT);
          t2.delete(oldT);
          tMap.delete(k as object);
        }
        else if (hasKey && typeof k === 'object' && k) {
          // handles a new object key

          const _t = watchGraph(
            callback, k, tail, nextTail
          );
          t2.add(_t);
          tMap.set(k, _t);
        }
      }
    }
  });

  for (const k of this.keys()) {
    if (typeof k === 'object' && k) {
      const _t = watchGraph(
        callback, k, tail, nextTail
      );
      t2.add(_t);
      tMap.set(k, _t);
    }
  }

  return [
    eachTerminator,
    watchKey(this, eachKeySymbol, callback),
    t2
  ];
};

export function mapSet <K, V, T extends Map<K, V>> (
  map: T, key: K, value: V
): void {
  if (map.get(key) !== value) {
    map.set(key, value);

    notifyChanges(map, [eachSymbol, eachKeySymbol], { k: key });
  }
}

export function mapDelete <K, T extends Map<K, unknown>> (
  map: T, key: K
): boolean {
  const result = map.delete(key);

  if (result) {
    notifyChanges(map, [eachSymbol, eachKeySymbol], { k: key });
  }

  return result;
}

export function mapClear <T extends Map<unknown, unknown>> (
  map: T,
): void {
  if (map.size) {
    map.clear();

    notifyChanges(map, [eachSymbol, eachKeySymbol], { t: MapChangeType.Clear });
  }
}

export class WatchableMap <K, V> extends Map<K, V> {
  set (key: K, value: V): this {
    if (this.get(key) !== value) {
      super.set(key, value);

      notifyChanges(this, [eachSymbol, eachKeySymbol], { k: key });
    }

    return this;
  }
  delete (key: K): boolean {
    const result = super.delete(key);

    if (result) {
      notifyChanges(this, [eachSymbol, eachKeySymbol], { k: key });
    }

    return result;
  }
  clear (): void {
    if (this.size) {
      super.clear();

      notifyChanges(this, [eachSymbol, eachKeySymbol], { t: MapChangeType.Clear });
    }
  }
}
