import { expect } from 'chai';

import { set } from './main';
import { startAgent, stopAgent } from './agent';
import { computed, watches } from './decorators';
import { Key } from './key-graph/types';

const wait = (ms: number) => {
  return new Promise<void>((resolve) => {
    setTimeout(resolve, ms);
  });
};

describe('Ticker Watcher', () => {
  class Ticker {
    count = 0;
    @watches('count')
    tick () {
      setTimeout(() => {
        set(this, 'count', this.count + 1);
      }, 1);
    }
  }

  let ticker = new Ticker();

  beforeEach(() => {
    startAgent(ticker);
  });
  afterEach(() => {
    stopAgent(ticker);
    ticker = new Ticker();
  });
  it('should tick', async () => {
    expect(ticker.count).to.equal(0);
    set(ticker, 'count', 1);
    expect(ticker.count).to.equal(1);
    await wait(10);
    expect(ticker.count).to.be.greaterThan(4);
  });
  it('should get one last tick after watcher is stopped', async () => {
    expect(ticker.count).to.equal(0);
    set(ticker, 'count', 1);
    expect(ticker.count).to.equal(1);
    await wait(10);
    const { count } = ticker;
    expect(count).to.be.greaterThan(4);
    stopAgent(ticker);
    await wait(10);
    expect(ticker.count).to.equal(count + 1);
  });
});

describe('Watcher with computed prop', () => {
  let count = 0;
  class Cacher {
    x = 0;
    @computed('x')
    get y () {
      count++;
      return this.x + count;
    }
    @computed('y')
    get yBox () {
      return { y: this.y };
    }
  }

  let cacher = new Cacher();

  beforeEach(() => {
    count = 0;
    startAgent(cacher);
  });
  afterEach(() => {
    stopAgent(cacher);
    cacher = new Cacher();
  });
  it('returns a cached value for the watching getter', () => {
    expect(cacher.y).to.equal(1);
    expect(cacher.y).to.equal(1);
  });
  it('invalidates cache when watched props change', () => {
    expect(cacher.y).to.equal(1);
    set(cacher, 'x', 1);
    expect(cacher.y).to.equal(3);
    expect(cacher.y).to.equal(3);
  });
  it('invalidates cache when watched computed prop invalidates', () => {
    const yB1 = cacher.yBox;
    expect(cacher.yBox).to.equal(yB1);
    set(cacher, 'x', 1);
    const yB2 = cacher.yBox;
    expect(yB2).to.not.equal(yB1);
    expect(cacher.yBox).to.equal(yB2);
  });
});

describe('Multiple watcher instances', () => {
  let count = 0;
  class Cacher {
    x = 0;
    @computed('x')
    get y () {
      count++;
      return this.x + count;
    }
  }

  let cacherA = new Cacher();
  let cacherB = new Cacher();

  beforeEach(() => {
    count = 0;
    startAgent(cacherA);
    startAgent(cacherB);
  });
  afterEach(() => {
    stopAgent(cacherA);
    stopAgent(cacherB);
    cacherA = new Cacher();
    cacherB = new Cacher();
  });
  it('returns a cached value for the watching getter', () => {
    expect(cacherA.y).to.equal(1);
    expect(cacherB.y).to.equal(2);
    expect(cacherA.y).to.equal(1);
    expect(cacherB.y).to.equal(2);
  });
  it('invalidates cache when watched props change', () => {
    expect(cacherA.y).to.equal(1);
    expect(cacherB.y).to.equal(2);
    set(cacherA, 'x', 1);
    set(cacherB, 'x', 2);
    expect(cacherA.y).to.equal(4);
    expect(cacherB.y).to.equal(6);
    expect(cacherA.y).to.equal(4);
    expect(cacherB.y).to.equal(6);
  });
});

describe('Watcher with custom computed prop', () => {
  function computedBool (key: Key) {
    const _comp = computed(key);
    return (target: object, _key: Key) => {
      Object.defineProperty(target, _key, _comp(target, _key, {
        get (): boolean {
          return !!this[key];
        },
        set (value: boolean) {
          return value;
        }
      }));
    };
  }

  class Cacher {
    x = 0;

    @computedBool('x')
    y!: boolean;

    @computedBool('y')
    z!: boolean;
  }

  it('should be able to get through the computed property', () => {
    const cacher = new Cacher();
    startAgent(cacher);

    expect(cacher.y).to.be.false;
    expect(cacher.z).to.be.false;
    set(cacher, 'x', 1);
    expect(cacher.y).to.be.true;
    expect(cacher.z).to.be.true;
  });

  it('should be able to set through the computed property', () => {
    const cacher = new Cacher();
    startAgent(cacher);

    set(cacher, 'y', true);
    expect(cacher.y).to.be.true;
    expect(cacher.z).to.be.true;
  });
});
