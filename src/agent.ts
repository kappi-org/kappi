import { Terminator } from './internals';

type Effect <T extends object> = (agent: T) => void | Terminator;
type EffectsMeta = Effect<any>[];

const effectsMetaKey = Symbol('__KAPPI_Effects_META__');

function retrieveEffectsMeta (agent: object) {
  const ownEffectsMetaDesc =
    Object.getOwnPropertyDescriptor(agent, effectsMetaKey) as { value: EffectsMeta } | undefined;

  if (ownEffectsMetaDesc) {
    return ownEffectsMetaDesc.value;
  }

  const protoEffectsMeta: EffectsMeta | undefined = agent[effectsMetaKey];
  const effectsMeta = protoEffectsMeta ?
    [...protoEffectsMeta] :
    [];
  Object.defineProperty(agent, effectsMetaKey, {
    value: effectsMeta,
  });

  return effectsMeta;
}


export function registerEffect <T extends object = object> (agent: T, effect: Effect<T>) {
  retrieveEffectsMeta(agent).push(effect);
}

export function registerEffects <T extends object = object> (agent: T, ...effects: Effect<T>[]) {
  retrieveEffectsMeta(agent).push(...effects);
}

const startedWatchers = new WeakMap<object, Terminator[]>();

export function startAgent (target: object) {
  const effectsMeta: EffectsMeta | undefined = target[effectsMetaKey];

  if (!effectsMeta) {
    return;
  }

  const terminators: Terminator[] = [];

  for (const effect of effectsMeta) {
    const t = effect(target);
    if (t) {
      terminators.push(t);
    }
  }

  startedWatchers.set(target, terminators);
}

export function stopAgent (target: object) {
  const terminators = startedWatchers.get(target);

  if (!terminators) {
    return;
  }

  startedWatchers.delete(target);

  for (const t of terminators) {
    t();
  }
}
