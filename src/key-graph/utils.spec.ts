import { expect } from 'chai';

import parseKeyGraph from './parse';
import { traverseGraph, objectFromGraph } from './utils';
import { Key } from './types';

describe('objectFromGraph(graph)', () => {
  const cases = [
    {
      graphString: '{a,b}.{c,d}.{e,f}',
      objFactory: () => {
        const cd = {
          e: {},
          f: {},
        };
        const ab = {
          c: cd,
          d: cd,
        };
        return {
          a : ab,
          b: ab,
        };
      }
    },
    {
      graphString: 'a,b.{c,d}.{e.f,g.{h,i},j}.k,l',
      objFactory: () => {
        const _k = { k: {} };
        const cd = {
          e: { f: _k },
          g: {
            h: _k,
            i: _k,
          },
          j: _k,
        };
        return {
          a : {},
          b: {
            c: cd,
            d: cd,
          },
          l: {},
        };
      }
    },
    {
      graphString: '{a,b.{c,d}.e}.{f,g.{h,i}.j}.k',
      objFactory: () => {
        const _k = { k: {} };
        const _j = { j: _k };
        const __k = {
          f: _k,
          g: {
            h: _j,
            i: _j,
          },
        };
        const _e = {
          e: __k
        };
        return {
          a : __k,
          b: {
            c: _e,
            d: _e,
          }
        };
      }
    }
  ];

  cases.forEach(({ graphString, objFactory }) => {
    context(`graph "${graphString}"`, () => {
      it('should make correct object', () => {
        const graph = parseKeyGraph(graphString);
        const obj = objectFromGraph(graph);

        expect(obj).to.deep.equal(objFactory());
      });
    });
  });

});

describe('traverseGraph(callback, object, graph)', () => {
  const cases = [
    {
      graphString: '{a,b}.{c,d}.{e,f}',
      objFactory: () => {
        const cd = {
          e: true,
          f: true,
        };
        const ab = {
          c: cd,
          d: cd,
        };
        return {
          a : ab,
          b: ab,
        };
      },
      /* eslint-disable @typescript-eslint/indent */
      keyOrder: [
        'a',
          'c',
            'e',
            'f',
          'd',
            'e',
            'f',
        'b',
          'c',
            'e',
            'f',
          'd',
            'e',
            'f',
      ]
      /* eslint-enable @typescript-eslint/indent */
    },
    {
      graphString: 'a,b.{c,d}.{e.f,g.{h,i},j}.k,l',
      objFactory: () => {
        const _k = { k: true };
        const cd = {
          e: { f: _k },
          g: {
            h: _k,
            i: _k,
          },
          j: _k,
        };
        return {
          a : true,
          b: {
            c: cd,
            d: cd,
          },
          l: true,
        };
      },
      /* eslint-disable @typescript-eslint/indent */
      keyOrder: [
        'a',
        'b',
          'c',
            'e',
              'f',
                'k',
            'g',
              'h',
                'k',
              'i',
                'k',
            'j',
              'k',
          'd',
            'e',
              'f',
                'k',
            'g',
              'h',
                'k',
              'i',
                'k',
            'j',
              'k',
        'l',
      ]
      /* eslint-enable @typescript-eslint/indent */
    },
    {
      graphString: '{a,b.{c,d}.e}.{f,g.{h,i}.j}.k',
      objFactory: () => {
        const _k = { k: true };
        const _j = { j: _k };
        const __k = {
          f: _k,
          g: {
            h: _j,
            i: _j,
          },
        };
        const _e = {
          e: __k
        };
        return {
          a : __k,
          b: {
            c: _e,
            d: _e,
          }
        };
      },
      /* eslint-disable @typescript-eslint/indent */
      keyOrder: [
        'a',
          'f',
            'k',
          'g',
            'h',
              'j',
                'k',
            'i',
              'j',
                'k',
        'b',
          'c',
            'e',
              'f',
                'k',
              'g',
                'h',
                  'j',
                    'k',
                'i',
                  'j',
                    'k',
          'd',
            'e',
              'f',
                'k',
              'g',
                'h',
                  'j',
                    'k',
                'i',
                  'j',
                    'k',
      ]
      /* eslint-enable @typescript-eslint/indent */
    }
  ];
  cases.forEach(({ graphString, objFactory, keyOrder }) => {
    context(`graph "${graphString}"`, () => {
      it('should traverse whole graph', () => {
        const graph = parseKeyGraph(graphString);
        const obj = objFactory();
        const testKeyOrder: (Key)[] = [];

        traverseGraph((o, key) => {
          testKeyOrder.push(key);
        }, obj, graph);

        expect(testKeyOrder).to.deep.equal(keyOrder);
      });
    });
  });
});
