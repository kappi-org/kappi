export type Key = string | symbol;

export interface KeyLink {
  head: KeyBranch | Key;
  tail: Key | KeyLink | KeyBranch;
}

export interface KeyBranch extends Set<Key | KeyLink> {}

export interface KeyPath {
  head: Key;
  tail: Key | KeyPath;
}

export interface RefKey {
  ref: Key;
}

export interface RefKeyLink {
  head: RefKey;
  tail: Key | KeyLink | KeyBranch;
}

export interface RefKeyPath {
  head: RefKey;
  tail: Key | KeyPath;
}

export interface RootKeyBranch extends Set<Key | KeyLink | RefKey | RefKeyPath> {}

export interface DynamicKey {
  dynamicRoot: Key | KeyPath | RefKey | RefKeyPath;
}

export type KeyGraph = Key | KeyLink | KeyBranch;

export type RootKeyGraph = Key | KeyLink | RefKey | RefKeyLink | RootKeyBranch;
